# -*- coding: utf-8 -*-
"""
Created on Fri Oct 02 21:10:33 2015

@author: Laptop
"""

import sys, os, re

def main():

    if len(sys.argv) < 2:
        sys.exit("Usage: %s filename" % sys.argv[0])

    baseball(sys.argv[1])
     
def baseball(filename):
    filename = sys.argv[1]
     
    if not os.path.exists(filename):
        sys.exit("Error: File '%s' not found" % sys.argv[1])
        
    print "baseball data\n"
        
    with open(filename,'r') as f:
        players = []
        bats = []
        hits = []
            
        
        for line in f.readlines():
            #print line
            if (line.find("=") != -1):
                continue
            if (line.find("#") != -1):
                continue
            if (len(line) < 2):
                continue
            
            
            players.append(re.search(r"^(\w+? \w+?)\b",line).group(1))
            bats.append(re.search(r"batted (\d+?) times",line).group(1))
            hits.append(re.search(r"with (\d+?) hits",line).group(1))           
            
    players_set = set(players)
    
    data_bats = {key: 0 for key in players_set}
    data_hits = {key: 0 for key in players_set}
    data_avg = {key: 0 for key in players_set}
    
    while (len(players)>0):
        player = players.pop()
        bat = bats.pop()
        hit = hits.pop()
        
        data_bats[player] += int(bat)
        data_hits[player] += int(hit)
    
    
    for player in sorted(players_set):
        data_avg[player] = float(data_hits[player])/float(data_bats[player])
    
    sorted_data = sorted(data_avg.items(),key= lambda x:x[1], reverse=True)
    for entry in sorted_data:
        print entry[0]+": %0.3f" % entry[1]
        
main()