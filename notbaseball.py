# -*- coding: utf-8 -*-
"""
Created on Mon Oct 05 11:45:03 2015

@author: Laptop
"""

import sys, re

def main():

    if len(sys.argv) < 2:
        sys.exit("Usage: %s filename" % sys.argv[0])

    print hello_world(sys.argv[1])
    print trip_vowel(sys.argv[1])
    print flight(sys.argv[1])
    
def hello_world(sentence):
    if re.search(r".*?\b(hello world)\b",sentence) is not None:
        return re.search(r".*?\b(hello world)\b",sentence).group(1)
    else:
        return ''
    
def trip_vowel(sentence):
    tv = re.compile(r"\b(\w*[aeiou]{3}.*?)\b")
    return tv.findall(sentence)

def flight(code):
    fl = re.compile(r"(AA\d{3}\d?)")
    return fl.findall(code)
    
main()